# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

25.times do |n|
  n = User.create!(first_name: Faker::Name.first_name,
               last_name: Faker::Name.last_name,
               email: "user#{n}@email.com",
               city: Faker::Address.city,
               date_of_birthday: Faker::Date.birthday(18, 65),
               password:              '123123',
               password_confirmation: '123123',
               )
  n.photos.attach(
      io: open("https://loremflickr.com/320/240?lock=#{n.id}"),
      filename: "#{n.id}.jpg"
  )
end

users = User.all
user  = users.first
following = users[2..12]
followers = users[3..25]
following.each { |followed| user.follow(followed) }
followers.each { |follower| follower.follow(user) }