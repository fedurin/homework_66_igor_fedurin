Rails.application.routes.draw do
  devise_for :users
  root "welcome#index"
  get '/profile' => 'users#show'
  put '/send' => 'devise/registrations#update'
  resources :users do
    member do
      get :following, :followers
      get :like, :unlike
      # post :comment
    end
  end
  resources :relationships, only: [:create, :destroy]
  resources :likes, only: [:create, :destroy]
  resources :comments, only: [:create]
end
