class LikesController < ApplicationController
  def create
    user = User.find(params[:blob_id])
    blob_id = ActiveStorage::Attachment.find(params[:blob_id])
    if Like.create(user_id: current_user.id, blob_id: blob_id.id)
      redirect_to user
    end
  end

  def destroy
    user = User.find(params[:id])
    blob_id = ActiveStorage::Attachment.find(params[:blob_id])
    @like = Like.all.find_by(user_id: current_user.id, blob_id: blob_id.id)
    if @like.destroy
      redirect_to user
    end
  end

  private
  def like_params
    params.require(:like).permit(:user_id, :blob_id)
  end
end