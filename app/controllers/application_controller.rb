class ApplicationController < ActionController::Base
  before_action :configure_permitted_parameters, if: :devise_controller?

  def logged_in_user
    unless logged_in?
      flash[:danger] = "Please log in."
      redirect_to new_user_session_path
    end
  end

  def logged_in?
    !current_user.nil?
  end

  private

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:first_name, :last_name, :date_of_birthday, :city, photos:[]])
    devise_parameter_sanitizer.permit(:account_update, keys: [:first_name, :last_name, :date_of_birthday, :city, photos:[]])
  end

end
