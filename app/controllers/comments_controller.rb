class CommentsController < ApplicationController

  def new
    @comment = Comment.new
  end
  def create
    @user = User.find(params[:user_id])
    @photo = ActiveStorage::Attachment.find(params[:blob_id])
    @comment = Comment.create!(user_id: current_user.id, blob_id: params[:blob_id], content: params[:comment][:content])
      redirect_to @user
  end

  private
  def comment_params
    params.require(:comment).permit(:content, :user_id, :blob_id)
  end
end